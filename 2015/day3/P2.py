cp = [0, 0, 0, 0]  # [scx,scy,rcx,rcy]
v = set()
with open("input.txt", "r") as f:
    for l in f:
        for i, c in enumerate(l):
            m = (i % 2) * 2
            if c == "^":
                cp[1 + m] += 1
            elif c == "v":
                cp[1 + m] -= 1
            elif c == ">":
                cp[0 + m] += 1
            elif c == "<":
                cp[0 + m] -= 1
            v.add(str(cp[1 + m]) + ":" + str(cp[0 + m]))

print(len(v))
