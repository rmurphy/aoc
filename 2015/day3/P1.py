cx, cy = 0, 0
v = set()
with open("input.txt", "r") as f:
    for l in f:
        for c in l:
            if c == "^":
                cy += 1
            elif c == "v":
                cy -= 1
            elif c == ">":
                cx += 1
            elif c == "<":
                cx -= 1
            v.add(str(cx) + ":" + str(cy))

print(len(v))
