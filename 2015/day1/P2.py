floor = 0
with open("input.txt", "r") as f:
    for line in f:
        for i, char in enumerate(line):
            if char == "(":
                floor += 1
            elif char == ")":
                floor -= 1
            if floor < 0:
                print(i + 1)
                exit()
