total_area = 0
with open("input.txt", "r") as f:
    for line in f:
        l, w, h = map(int, line.strip().split("x"))
        a = l * w
        b = w * h
        c = h * l
        minimum = min(a, min(b, c))
        area = 2 * (a + b + c)
        total_area += area + minimum

print(total_area)
