total_len = 0
with open("input.txt", "r") as f:
    for line in f:
        l, w, h = map(int, line.strip().split("x"))
        # minn = min(l, min(w, h))
        maxx = max(l, max(w, h))
        summ = l + w + h
        total_len += 2 * (summ - maxx) + l * w * h

print(total_len)
