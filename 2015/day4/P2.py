nice = 0
with open("input-t.txt", "r") as f:
    for l in f:
        letter_count = {}
        two_same = False
        lc = ""
        dups = {}
        for c in l.strip():
            if c in letter_count.keys():
                letter_count[c] += 1
            else:
                letter_count[c] = 1

            if lc != "":
                if lc + c in dups.keys():
                    dups[lc + c] += 1
                else:
                    dups[lc + c] = 1
            lc = c
        print(dups)

        for key, value in letter_count.items():
            if value == 2:
                two_same = True

        # for key, value in dups.items():
        #    if value >= 2:
        #       dups = True
        if two_same:  # and dups:
            nice += 1
print(nice)
