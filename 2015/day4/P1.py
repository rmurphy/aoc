vs = "aeiou"
nice = 0
with open("input.txt", "r") as f:
    for l in f:
        vc = 0
        lc = ""
        dup = False
        bad = False
        for c in l:
            if c == "b":
                if lc == "a":
                    bad = True
            if c == "d":
                if lc == "c":
                    bad = True
            if c == "q":
                if lc == "p":
                    bad = True
            if c == "y":
                if lc == "x":
                    bad = True
            if c == lc:
                dup = True
            if c in "aeiou":
                vc += 1
            lc = c
        if dup and vc >= 3 and not bad:
            nice += 1
print(nice)
