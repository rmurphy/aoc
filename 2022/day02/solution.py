def part1(input_file_name):
    total = 0
    with open(input_file_name, "r") as f:
        for l in f:
            game = l.split()
            total += score_p1(game)
    return total


def part2(input_file_name):
    total = 0
    with open(input_file_name, "r") as f:
        for l in f:
            game = l.split()
            total += score_p2(game)
    return total


def score_p1(g):
    w = 6
    l = 0
    d = 3
    p = {"X": 1, "Y": 2, "Z": 3}
    o = {
        "A": {"X": d, "Y": w, "Z": l},
        "B": {"X": l, "Y": d, "Z": w},
        "C": {"X": w, "Y": l, "Z": d},
    }
    return p[g[1]] + o[g[0]][g[1]]


def score_p2(g):
    a = 1
    b = 2
    c = 3
    p = {"X": 0, "Y": 3, "Z": 6}
    o = {
        "A": {"X": c, "Y": a, "Z": b},
        "B": {"X": a, "Y": b, "Z": c},
        "C": {"X": b, "Y": c, "Z": a},
    }
    return p[g[1]] + o[g[0]][g[1]]


if __name__ == "__main__":
    input_file_name = "input.txt"
    # input_file_name = "test.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
