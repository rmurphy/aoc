def part1(input_file_name):
    total = 0
    with open(input_file_name, "r") as f:
        for l in f:
            l = l.strip()
            c = ord(set(l[: len(l) // 2]).intersection(set(l[len(l) // 2 :])).pop())
            total += c - 96 if c > 90 else c - 38
    return total


def part2(input_file_name):
    total = 0
    t = [""] * 3
    with open(input_file_name, "r") as f:
        for n, l in enumerate(f):
            t[n % 3] = set(l.strip())
            if n % 3 == 2:
                b = ord(t[0].intersection(t[1]).intersection(t[2]).pop())
                total += b - 96 if b > 90 else b - 38
    return total


if __name__ == "__main__":
    input_file_name = "input.txt"
    # input_file_name = "test.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
