import math


def part1(input_file_name):
    maxx = summ = 0
    with open(input_file_name, "r") as f:
        for l in f:
            l = l.strip()
            if l == "":
                maxx = max(summ, maxx)
                summ = 0
                continue
            summ += int(l)
        summ = max(summ, maxx)

    return maxx


def part2(input_file_name):
    tm = [-math.inf] * 3
    summ = 0
    with open(input_file_name, "r") as f:
        for l in f:
            l = l.strip()
            if l == "":
                tm = max3(tm, summ)
                summ = 0
                continue
            summ += int(l)
        tm = max3(tm, summ)
    return sum(tm)


def max3(tm, n):
    if n > tm[0]:
        return [n] + tm[0:2]
    if n > tm[1]:
        return [tm[0], n, tm[1]]
    if n > tm[2]:
        return tm[0:2] + [n]
    return tm


if __name__ == "__main__":
    input_file_name = "input.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
