import json


def part1(input_file_name):
    fs = {}
    fs.update({"..": fs})
    pwd = fs
    with open(input_file_name, "r") as f:
        for l in f:
            sl = l.split()
            if sl[0] != "$":
                if sl[0] == "dir":
                    pwd.update({sl[1]: {"..": pwd}})
                else:
                    pwd.update({sl[1]: sl[0]})
            else:
                if sl[1] == "cd":
                    if sl[2] == "/":
                        pwd = fs
                    else:
                        pwd = pwd.get(sl[2])
    print_fs(fs)
    return 0


def part2(input_file_name):
    return 0


def print_fs(fs):
    jsont = str(fs).replace("{...}", "'{...}'").replace("'", '"')
    print(json.dumps(json.loads(jsont), sort_keys=True, indent=2))


if __name__ == "__main__":
    input_file_name = "input.txt"
    input_file_name = "test.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
