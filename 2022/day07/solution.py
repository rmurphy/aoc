import json
import math


def part1(input_file_name):
    fs = {"_files": [], "_dirs": {}}
    fs.get("_dirs").update({"..": fs})
    pwd = fs
    with open(input_file_name, "r") as f:
        for l in f:
            sl = l.split()
            if sl[0] != "$":
                if sl[0] == "dir":
                    pwd.get("_dirs").update(
                        {sl[1]: {"_files": [], "_dirs": {"..": pwd}}}
                    )
                else:
                    pwd.get("_files").append(int(sl[0]))
            else:
                if sl[1] == "cd":
                    if sl[2] == "/":
                        pwd = fs
                    else:
                        pwd = pwd.get("_dirs").get(sl[2])

    def cs(fs, ltd):
        sf = sum(fs.get("_files"))
        sd = 0
        for d in fs.get("_dirs").items():
            if d[0] != "..":
                sd += cs(d[1], ltd)
        ts = sf + sd
        fs.update({"_size": ts})
        if ts <= 100000:
            ltd.append(ts)
        return ts

    ltd = []
    cs(fs, ltd)
    # print_fs(fs)
    return sum(ltd)


def part2(input_file_name):
    fs = {"_files": [], "_dirs": {}}
    fs.get("_dirs").update({"..": fs})
    pwd = fs
    with open(input_file_name, "r") as f:
        for l in f:
            sl = l.split()
            if sl[0] != "$":
                if sl[0] == "dir":
                    pwd.get("_dirs").update(
                        {sl[1]: {"_files": [], "_dirs": {"..": pwd}}}
                    )
                else:
                    pwd.get("_files").append(int(sl[0]))
            else:
                if sl[1] == "cd":
                    if sl[2] == "/":
                        pwd = fs
                    else:
                        pwd = pwd.get("_dirs").get(sl[2])

    def cs(fs):
        sf = sum(fs.get("_files"))
        sd = 0
        for d in fs.get("_dirs").items():
            if d[0] != "..":
                sd += cs(d[1])
        ts = sf + sd
        fs.update({"_size": ts})
        return ts

    cs(fs)
    # print_fs(fs)
    ntf = fs.get("_size") - (70000000 - 30000000)

    def fm(fs, td):
        s = fs.get("_size")
        if s >= ntf:
            td = min(td, s)
        for d in fs.get("_dirs").items():
            if d[0] != "..":
                td = fm(d[1], td)
        return td

    td = math.inf
    return fm(fs, td)


def print_fs(fs):
    print(
        json.dumps(
            json.loads(str(fs).replace("{...}", "'{...}'").replace("'", '"')),
            sort_keys=True,
            indent=2,
        )
    )


if __name__ == "__main__":
    input_file_name = "input.txt"
    # input_file_name = "test.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
