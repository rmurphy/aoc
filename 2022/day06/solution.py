def part1(input_file_name):
    with open(input_file_name, "r") as f:
        l = f.readline()
    i = 0
    while len(set(l[i : i + 4])) != 4:
        i += 1
    return i + 4


def part2(input_file_name):
    with open(input_file_name, "r") as f:
        l = f.readline()
    i = 0
    while len(set(l[i : i + 14])) != 14:
        i += 1
    return i + 14


if __name__ == "__main__":
    input_file_name = "input.txt"
    # input_file_name = "test.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
