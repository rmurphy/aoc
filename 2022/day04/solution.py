def part1(input_file_name):
    total = 0
    with open(input_file_name, "r") as f:
        for l in f:
            l = [[int(x) for x in ll.split("-")] for ll in l.strip().split(",")]
            if (l[0][0] <= l[1][0] and l[0][1] >= l[1][1]) or (
                l[1][0] <= l[0][0] and l[1][1] >= l[0][1]
            ):
                total += 1
    return total


def part2(input_file_name):
    total = 0
    with open(input_file_name, "r") as f:
        for l in f:
            l = [[int(x) for x in ll.split("-")] for ll in l.strip().split(",")]
            if (
                (l[0][0] >= l[1][0] and l[0][0] <= l[1][1])
                or (l[0][1] >= l[1][0] and l[0][1] <= l[1][1])
                or (l[1][0] >= l[0][0] and l[1][0] <= l[0][1])
                or (l[1][1] >= l[0][0] and l[1][1] <= l[0][1])
            ):
                print(l)
                total += 1
    return total


if __name__ == "__main__":
    input_file_name = "input.txt"
    # input_file_name = "test.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
