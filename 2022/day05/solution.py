import re


def part1(input_file_name):
    result = ""
    with open(input_file_name, "r") as f:
        ls = f.readlines()
    i = 0
    while not re.match("^ *[0-9][0-9 ]*$", ls[i]):
        i += 1
    cs = [m.start() for m in re.finditer("[0-9]", ls[i])]
    ss = [[] for _ in cs]
    for l in reversed(ls[:i]):
        for j, k in enumerate(cs):
            if l[k] != " ":
                ss[j].append(l[k])
    for l in ls[i + 2 :]:
        m = [int(n) for n in re.findall("\d+", l)]
        for _ in range(m[0]):
            if len(ss[m[1] - 1]) != 0:
                ss[m[2] - 1].append(ss[m[1] - 1].pop())
    for s in ss:
        if len(s) != 0:
            result += s.pop()
    return result


def part2(input_file_name):
    result = ""
    with open(input_file_name, "r") as f:
        ls = f.readlines()
    i = 0
    while not re.match("^ *[0-9][0-9 ]*$", ls[i]):
        i += 1
    cs = [m.start() for m in re.finditer("[0-9]", ls[i])]
    ss = [[] for _ in cs]
    for l in reversed(ls[:i]):
        for j, k in enumerate(cs):
            if l[k] != " ":
                ss[j].append(l[k])
    for l in ls[i + 2 :]:
        m = [int(n) for n in re.findall("\d+", l)]
        ss[m[2] - 1] = ss[m[2] - 1] + ss[m[1] - 1][-m[0] :]
        ss[m[1] - 1] = ss[m[1] - 1][: -m[0]]
    for s in ss:
        if len(s) != 0:
            result += s.pop()
    return result


if __name__ == "__main__":
    input_file_name = "input.txt"
    # input_file_name = "test.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
