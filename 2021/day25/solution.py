import sys, pprint


def part1(d):
    m = True
    c = 0
    while m:
        # gp(d)
        # print("-" * len(d[0]))
        dp = ac(d)
        m = False
        c += 1
        xs = False
        ys = []
        for y in range(len(d)):
            for x in range(len(d[0])):
                if d[y][x] == ">" and d[y][(x + 1) % len(d[0])] == ".":
                    dp[y][x] = "."
                    dp[y][(x + 1) % len(d[0])] = ">"
                    m = True
                    xs = True
                elif xs:
                    xs = False
                else:
                    dp[y][x] = d[y][x]
        d = ac(dp)
        for y in range(len(d)):
            for x in range(len(d[0])):
                if d[y][x] == "v" and d[(y + 1) % len(d)][x] == ".":
                    dp[y][x] = "."
                    dp[(y + 1) % len(d)][x] = "v"
                    m = True
                    ys.append((x, y + 1))
                elif (x, y) in ys:
                    continue
                else:
                    dp[y][x] = d[y][x]
        d = ac(dp)
    return c


def gp(d):
    for r in d:
        print("".join(r))


def ac(a):
    b = []
    for c in a:
        b.append(c.copy())
    return b


def part2(d):
    pass


def load_input(fn):
    data = []
    with open(fn, "r") as f:
        for l in f:
            data.append(list(l.strip()))
    return data


def main(input_file_name):
    data = load_input(input_file_name)
    print("Part1: " + str(part1(data)))
    print("Part2: " + str(part2(data)))


if __name__ == "__main__":
    main(sys.argv[1])
