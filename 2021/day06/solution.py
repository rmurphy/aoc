import time


def timed(func):
    def wrapper(*args, **kw):
        start = time.time()
        result = func(*args, **kw)
        end = time.time()
        print("Execution of " + func.__name__ + " time in seconds: " + str(end - start))
        return result

    return wrapper


@timed
def without_mod(initial, days):
    fish = [0] * 9
    m = 0
    n = 7
    for days_left in initial:
        fish[days_left] += 1
    for _ in range(days):
        fish[n] += fish[m]
        m = 0 if m == 8 else m + 1
        n = m + 7 if m + 7 < 9 else m - 2
    return sum(fish)


@timed
def with_mod(initial, days):
    fish = [0] * 9
    for days_left in initial:
        fish[days_left] += 1
    for i in range(days):
        m = i % 9
        n = (i + 7) % 9
        fish[n] += fish[m]

    return sum(fish)


@timed
def slice_one_list(initial, days):
    fish = [0] * 9
    for days_left in initial:
        fish[days_left] += 1
    for _ in range(days):
        fish0 = fish[0]
        fish = fish[1:9] + [fish0]
        fish[6] += fish0

    return sum(fish)


if __name__ == "__main__":
    initials = list(map(int, open("input.txt").readline().split(",")))

    # Testing speed on implementations
    # without_mod(initials, 2**19)
    # with_mod(initials, 2**19)
    # slice_one_list(initials, 2**19)

    print(with_mod(initials, 80))
    print(with_mod(initials, 256))
