"""
 aaaa    ....    aaaa    aaaa    ....
b    c  .    c  .    c  .    c  b    c
b    c  .    c  .    c  .    c  b    c
 ....    ....    dddd    dddd    dddd
e    f  .    f  e    .  .    f  .    f
e    f  .    f  e    .  .    f  .    f
 gggg    ....    gggg    gggg    ....

  5:      6:      7:      8:      9:
 aaaa    aaaa    aaaa    aaaa    aaaa
b    .  b    .  .    c  b    c  b    c
b    .  b    .  .    c  b    c  b    c
 dddd    dddd    ....    dddd    dddd
.    f  e    f  .    f  e    f  .    f
.    f  e    f  .    f  e    f  .    f
 gggg    gggg    ....    gggg    gggg

abef

n of seg
 0 = 6 
 1 = 2*
 2 = 5
 3 = 5
 4 = 4*
 5 = 5
 6 = 6
 7 = 3*
 8 = 7*
 9 = 6

n that contain
 a = 8
 b = 6*
 c = 8
 d = 7
 e = 4*
 f = 9*
 g = 7

"""


def part1(input_file_name):
    solution = 0
    with open(input_file_name, "r") as input_file:
        for line in input_file:
            signals, results = line.split("|")
            signals = signals.split()
            results = results.split()
            for signal in results:
                signal_length = len(signal)
                if (
                    signal_length == 2
                    or signal_length == 3
                    or signal_length == 4
                    or signal_length == 7
                ):
                    solution += 1
    return solution


def part2(input_file_name):
    solution = 0
    with open(input_file_name, "r") as input_file:
        for line in input_file:
            signal_map = [""] * 10
            segment_map = {}
            char_count = {}

            signals, results = line.split("|")

            for char in "abcdefg":
                char_count[char] = signals.count(char)

            signals = signals.split()
            results = results.split()

            for char, count in char_count.items():
                if count == 6:
                    segment_map["b"] = char
                if count == 4:
                    segment_map["e"] = char
                if count == 9:
                    segment_map["f"] = char
            signals_processed = []
            for i, signal in enumerate(signals):
                signal_length = len(signal)
                if signal_length == 2:
                    signal_map[1] = "".join(sorted(signal))
                    signals_processed.append(i)
                elif signal_length == 3:
                    signal_map[7] = "".join(sorted(signal))
                    signals_processed.append(i)
                elif signal_length == 4:
                    signal_map[4] = "".join(sorted(signal))
                    signals_processed.append(i)
                elif signal_length == 7:
                    signal_map[8] = "".join(sorted(signal))
                    signals_processed.append(i)
                elif signal_length == 5 and segment_map["b"] in signal:
                    signal_map[5] = "".join(sorted(signal))
                    signals_processed.append(i)
                elif signal_length == 5 and segment_map["f"] not in signal:
                    signal_map[2] = "".join(sorted(signal))
                    signals_processed.append(i)
            for i, signal in enumerate(signals):
                if len(signal) == 6:
                    if signal_map[1][0] not in signal or signal_map[1][1] not in signal:
                        signal_map[6] = "".join(sorted(signal))
                        signals_processed.append(i)
                    elif segment_map["e"] not in signal:
                        signal_map[9] = "".join(sorted(signal))
                        signals_processed.append(i)
                    else:
                        signal_map[0] = "".join(sorted(signal))
                        signals_processed.append(i)
            for index_to_del in sorted(signals_processed, reverse=True):
                del signals[index_to_del]
            signal_map[3] = "".join(sorted(signals[0]))

            # s['a'] = ''.join(set(m[7])-set(m[1])) #not needed, all signals decoded

            decoded_signals = ""
            for signal in results:
                decoded_signals += str(signal_map.index("".join(sorted(signal))))
            solution += int(decoded_signals)
    return solution


if __name__ == "__main__":
    input_file = "input.txt"
    print("Part1: " + str(part1(input_file)))
    print("Part2: " + str(part2(input_file)))
