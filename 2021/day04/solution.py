def load_input_file(input_file_name):
    boards = []
    with open("input.txt", "r") as input_file:
        draws = input_file.readline().strip().split(",")
        i = 0
        for line in input_file:
            if line == "\n":
                continue
            modulus = i % 5
            if modulus == 0:
                new_board = [{}, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
            for j, n in enumerate(line.strip().split()):
                new_board[0][n] = (modulus, j)
            if modulus == 4:
                boards.append(new_board)
            i += 1
    return boards, draws


def part1(input_file_name):
    boards, draws = load_input_file(input_file_name)
    for draw in draws:
        for board in boards:
            if draw in board[0]:
                position = board[0][draw]
                board[1][position[0] + 5] += 1
                board[1][position[1]] += 1
                del board[0][draw]
                if 5 in board[1]:
                    return int(draw) * sum(map(int, board[0].keys()))
                    break
        else:
            continue
        break


def part2(input_file_name):
    boards, draws = load_input_file(input_file_name)
    winning_board = (0, 0, -1)
    for board in boards:
        for o, draw in enumerate(draws):
            if draw in board[0]:
                position = board[0][draw]
                board[1][position[0] + 5] += 1
                board[1][position[1]] += 1
                del board[0][draw]
                if 5 in board[1]:
                    if o >= winning_board[2]:
                        winning_board = (board[0], draw, o)
                    break

    return int(winning_board[1]) * sum(map(int, winning_board[0].keys()))


if __name__ == "__main__":
    input_file_name = "input.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
