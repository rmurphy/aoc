import sys


def part1(iea, image):
    return ein(iea, image, 2)


def part2(iea, image):
    return ein(iea, image, 50)


def ein(iea, image, n):
    if n % 2 == 1:
        return "INFINITY"
    for i in range(n):
        if i % 2 == 1 and iea[0] == "#" and iea[-1] == ".":
            pc = "#"
        else:
            pc = "."
        image = ei(iea, image, pc)
    return "".join(image).count("#")


def ei(iea, image, pc):
    image = pad_image(image, pc)
    enhanced_image = []
    for y, r in enumerate(image):
        nl = ""
        for x, _ in enumerate(r):
            bn = ""
            for p in (
                (-1, -1),
                (0, -1),
                (1, -1),
                (-1, 0),
                (0, 0),
                (1, 0),
                (-1, 1),
                (0, 1),
                (1, 1),
            ):
                if (not (0 < x + p[0] < len(image[0]))) or (
                    not (0 < y + p[1] < len(image))
                ):
                    bn += pc
                else:
                    bn += image[y + p[1]][x + p[0]]

            bn = int(bn.replace(".", "0").replace("#", "1"), 2)
            nl += iea[bn]
        enhanced_image.append(nl)
    return enhanced_image


def load_input(fn):
    image = []
    with open(fn, "r") as f:
        iea = f.readline().strip()
        f.readline()
        for l in f:
            image.append(l.strip())
    if iea[0] == "#" and iea[-1] == "#":
        print("Infinity for all images")
        exit(0)
    return iea, image


def pad_image(image, pc):
    padded_image = []
    padded_image.append(pc * (len(image[0]) + 2))
    for l in image:
        padded_image.append(pc + l + pc)
    padded_image.append(pc * (len(image[0]) + 2))
    return padded_image


def main(input_file_name):
    iea, image = load_input(input_file_name)
    print("Part1: " + str(part1(iea, image)))
    print("Part2: " + str(part2(iea, image)))


if __name__ == "__main__":
    main(sys.argv[1])
