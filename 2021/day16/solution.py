import sys
import math
import operator

op = {
    "000": sum,
    "001": math.prod,
    "010": min,
    "011": max,
    "101": lambda n: operator.gt(*n),
    "110": lambda n: operator.lt(*n),
    "111": lambda n: operator.eq(*n),
}


def rp(d, o):
    h = d[o : o + 3], d[o + 3 : o + 6]
    if h[1] == "100":
        n = ""
        i = 0
        while True:
            s = d[o + 6 + i * 5 : o + 11 + i * 5]
            n += s[1:]
            if s[0] == "0":
                break
            i += 1
        return o + 11 + i * 5, int(h[0], 2), int(n, 2)
    else:
        vs = int(h[0], 2)
        es = []
        if d[o + 6] == "0":
            l = int(d[o + 7 : o + 22], 2)
            u = o + 22
            while u < o + 22 + l:
                u, v, e = rp(d, u)
                vs += v
                es.append(e)
        else:
            n = int(d[o + 7 : o + 18], 2)
            u = o + 18
            for _ in range(n):
                u, v, e = rp(d, u)
                vs += v
                es.append(e)
        return u, vs, int(op[h[1]](es))


def chtb(s):
    ns = ""
    for c in s:
        ns += bin(int(c, 16))[2:].zfill(4)
    return ns


def main(ifn):
    d = open(ifn).read().strip()
    d = chtb(d)
    _, v, e = rp(d, 0)
    print("Part 1: " + str(v))
    print("Part 2: " + str(e))


if __name__ == "__main__":
    main(sys.argv[1])
