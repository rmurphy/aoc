def part1(input_file_name):
    expanded_points = {}
    start = x = 0  # for understandability
    end = y = 1  # for understandability
    with open(input_file_name, "r") as input_file:
        for line in input_file:
            points = [
                [int(xy) for xy in se.strip().split(",")] for se in line.split("->")
            ]
            if points[start][x] == points[end][x]:
                for point in [
                    (points[start][x], q)
                    for q in range(
                        min(points[start][y], int(points[end][y])),
                        max(points[start][y], points[end][y]) + 1,
                    )
                ]:
                    if point in expanded_points:
                        expanded_points[point] += 1
                    else:
                        expanded_points[point] = 1
            elif points[start][y] == points[end][y]:
                for point in [
                    (q, points[start][y])
                    for q in range(
                        min(points[start][x], points[end][x]),
                        max(points[start][x], points[end][x]) + 1,
                    )
                ]:
                    if point in expanded_points:
                        expanded_points[point] += 1
                    else:
                        expanded_points[point] = 1
            else:
                pass  # probably used later

    result = 0
    for point in expanded_points:
        if expanded_points[point] >= 2:
            result += 1
    return result


def part2(input_file_name):
    expanded_points = {}
    start = x = 0  # for understandability
    end = y = 1  # for understandability
    with open(input_file_name, "r") as input_file:
        for line in input_file:
            points = [
                [int(xy) for xy in se.strip().split(",")] for se in line.split("->")
            ]
            if points[start][x] == points[end][x]:
                for point in [
                    ((points[start][x]), q)
                    for q in range(
                        min((points[start][y]), (points[end][y])),
                        max((points[start][y]), (points[end][y])) + 1,
                    )
                ]:
                    if point in expanded_points:
                        expanded_points[point] += 1
                    else:
                        expanded_points[point] = 1
            elif points[start][y] == points[end][y]:
                for point in [
                    (q, (points[start][y]))
                    for q in range(
                        min((points[start][x]), (points[end][x])),
                        max((points[start][x]), (points[end][x])) + 1,
                    )
                ]:
                    if point in expanded_points:
                        expanded_points[point] += 1
                    else:
                        expanded_points[point] = 1
            else:
                if (points[start][x]) < (points[end][x]) and (points[start][y]) < (
                    points[end][y]
                ):
                    for point in [
                        ((points[start][x]) + i, (points[start][y]) + i)
                        for i in range(((points[end][x]) - (points[start][x])) + 1)
                    ]:
                        if point in expanded_points:
                            expanded_points[point] += 1
                        else:
                            expanded_points[point] = 1
                elif (points[start][x]) < (points[end][x]) and (points[start][y]) > (
                    points[end][y]
                ):
                    for point in [
                        ((points[start][x]) + i, (points[start][y]) - i)
                        for i in range(((points[end][x]) - (points[start][x])) + 1)
                    ]:
                        if point in expanded_points:
                            expanded_points[point] += 1
                        else:
                            expanded_points[point] = 1
                elif (points[start][x]) > (points[end][x]) and (points[start][y]) > (
                    points[end][y]
                ):
                    for point in [
                        ((points[start][x]) - i, (points[start][y]) - i)
                        for i in range(((points[start][x]) - (points[end][x])) + 1)
                    ]:
                        if point in expanded_points:
                            expanded_points[point] += 1
                        else:
                            expanded_points[point] = 1
                elif (points[start][x]) > (points[end][x]) and (points[start][y]) < (
                    points[end][y]
                ):
                    for point in [
                        ((points[start][x]) - i, (points[start][y]) + i)
                        for i in range(((points[start][x]) - (points[end][x])) + 1)
                    ]:
                        if point in expanded_points:
                            expanded_points[point] += 1
                        else:
                            expanded_points[point] = 1
    result = 0
    for point in expanded_points:
        if expanded_points[point] >= 2:
            result += 1
    return result


if __name__ == "__main__":
    input_file_name = "input.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
