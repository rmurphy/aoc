import sys


def solve(pm, s, be, r):
    for _ in range(r):
        sp = {}
        for k, v in pm.items():
            if k in s:
                c = s[k]
                if v[0] in sp:
                    sp[v[0]] += c
                else:
                    sp[v[0]] = c
                if v[1] in sp:
                    sp[v[1]] += c
                else:
                    sp[v[1]] = c
        s = sp
    lc = {}
    for k, v in s.items():
        if list(k)[0] in lc:
            lc[list(k)[0]] += v
        else:
            lc[list(k)[0]] = v
        if list(k)[1] in lc:
            lc[list(k)[1]] += v
        else:
            lc[list(k)[1]] = v
    for l in be:
        lc[l] += 1
    for k, v in lc.items():
        lc[k] = int(v / 2)
    return lc[max(lc, key=lc.get)] - lc[min(lc, key=lc.get)]


def load_input(input_file_name):
    pm = {}
    s = {}
    be = []

    with open(input_file_name) as f:
        l = f.readline().strip()
        be = [l[0], l[-1]]
        for i in range(len(l) - 1):
            if l[i] + l[i + 1] in s:
                s[l[i] + l[i + 1]] += 1
            else:
                s[l[i] + l[i + 1]] = 1
        l = f.readline()
        pm = {
            l.strip().split(" -> ")[0]: [
                list(l.strip().split(" -> ")[0])[0] + l.strip().split(" -> ")[1],
                l.strip().split(" -> ")[1] + list(l.strip().split(" -> ")[0])[1],
            ]
            for l in f
        }
    return pm, s, be


def main(input_file_name):
    pm, s, be = load_input(input_file_name)
    print("Part1: " + str(solve(pm, s, be, 10)))
    print("Part2: " + str(solve(pm, s, be, 40)))


if __name__ == "__main__":
    main(sys.argv[1])
