import sys


def gp(g, w):
    for i in range(10):
        print(g[i * w : i * w + w])


def part1(input_file_name):
    im = open(input_file_name).read().replace("\n", "")
    im = [int(c) for c in im]
    flashes = 0
    no_flash = True
    for _ in range(100):
        while True:
            no_flash = True
            for j in range(len(im)):
                if im[j] > 8:
                    bl = j // 10 * 10
                    bu = (j // 10 + 1) * 10
                    no_flash = False
                    im[j] = -1
                    flashes += 1
                    for q in [-11, -10, -9, -1, 1, 9, 10, 11]:
                        jq = j + q
                        if (
                            jq < 0
                            or jq >= len(im)
                            or (q < -1 and (jq < bl - 10 or jq >= bu - 10))
                            or (q > 1 and (jq < bl + 10 or jq >= bu + 10))
                            or ((q == -1 or q == 1) and (jq < bl or jq >= bu))
                        ):
                            continue
                        if im[jq] > -1:
                            im[jq] += 1
            if no_flash:
                break
        for j in range(len(im)):
            im[j] += 1
    return flashes


def part2(input_file_name):
    im = open(input_file_name).read().replace("\n", "")
    im = [int(c) for c in im]
    rounds = 0
    no_flash = True
    while True:
        rounds += 1
        while True:
            no_flash = True
            for j in range(len(im)):
                if im[j] > 8:
                    bl = j // 10 * 10
                    bu = (j // 10 + 1) * 10
                    no_flash = False
                    im[j] = -1
                    for q in [-11, -10, -9, -1, 1, 9, 10, 11]:
                        jq = j + q
                        if (
                            jq < 0
                            or jq >= len(im)
                            or (q < -1 and (jq < bl - 10 or jq >= bu - 10))
                            or (q > 1 and (jq < bl + 10 or jq >= bu + 10))
                            or ((q == -1 or q == 1) and (jq < bl or jq >= bu))
                        ):
                            continue
                        if im[jq] > -1:
                            im[jq] += 1
            if no_flash:
                break
        for j in range(len(im)):
            im[j] += 1
        if sum(im) == 0:
            return rounds


def main(input_file_name):
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))


if __name__ == "__main__":
    main(sys.argv[1])
