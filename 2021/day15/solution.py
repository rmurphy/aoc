import sys
import heapq


def solve(d):
    h = [(0, 0)]
    d[0][0] = -1
    while h:
        c, p = heapq.heappop(h)
        if p == len(d[0]) - 1:
            return c
        for a in d[1]:
            if (a == 1 or a == -1) and (
                p + a < p // d[2] * d[2] or p + a >= (p // d[2] + 1) * d[2]
            ):
                continue
            if p + a >= 0 and p + a < len(d[0]) and d[0][p + a] != -1:
                heapq.heappush(h, (c + d[0][p + a], p + a))
                d[0][p + a] = -1


def load_input(input_file_name, m):
    data = []
    mx = my = 0
    with open(input_file_name, "r") as f:
        for l in f:
            my += 1
            mx = len(l.strip()) * m
            for i in range(m):
                for c in l.strip():
                    data.append((((int(c) + i) - 1) % 9) + 1)
        b = len(data)
        for i in range(1, m):
            data += [(((int(c) + i) - 1) % 9) + 1 for c in data[:b]]
    return (
        data,
        (-mx, mx, -1, 1),
        mx,
    )  # with diagonals (-(mx+1), -mx, -(mx-1), -1, 1, mx-1, mx, mx+1)


def main(input_file_name):
    data = load_input(input_file_name, 1)
    print("Part1: " + str(solve(data)))
    data = load_input(input_file_name, 5)
    print("Part2: " + str(solve(data)))


if __name__ == "__main__":
    main(sys.argv[1])
