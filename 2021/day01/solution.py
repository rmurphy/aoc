def part1(input_file_name):
    result = 0
    last = 2 ** 8  # start with arbitrary large number
    with open(input_file_name, "r") as f:
        for l in f:
            if int(l) > last:
                result += 1
            last = int(l)

    return result


def part2(input_file_name):
    result = 0
    last3 = (0, 0, 0)
    last_sum = 0
    i = 0
    with open("input.txt", "r") as f:
        for l in f:
            last3 = (int(l),) + last3[:2]
            last3_sum = sum(last3)
            if i > 3 and last3_sum > last_sum:
                result += 1
            last_sum = last3_sum
            i += 1

    return result


if __name__ == "__main__":
    input_file_name = "input.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
