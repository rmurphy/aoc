def part1(data):
    m = {")": "(", "]": "[", "}": "{", ">": "<"}
    v = {")": 3, "]": 57, "}": 1197, ">": 25137}
    t = 0
    for line in data:
        l = []
        for c in line:
            if c in "<([{":
                l.append(c)
            elif c in "}])>":
                if l.pop(-1) != m[c]:
                    t += v[c]
    return t


def remove_corrupt(data):
    m = {")": "(", "]": "[", "}": "{", ">": "<"}
    v = {")": 3, "]": 57, "}": 1197, ">": 25137}
    nc = []
    for line in data:
        t = 0
        l = []
        for c in line:
            if c in "<([{":
                l.append(c)
            elif c in "}])>":
                if l.pop(-1) != m[c]:
                    t += v[c]
        if t == 0:
            nc.append(line)
    return nc


def part2(data):
    data = remove_corrupt(data)
    m = {"(": ")", "[": "]", "{": "}", "<": ">"}
    v = {"(": 1, "[": 2, "{": 3, "<": 4}
    ts = []
    for line in data:
        t = 0
        line = list(line.strip())
        i = 0
        while i < len(line) - 1:
            if line[i] in "<([{" and line[i + 1] == m[line[i]]:
                del line[i + 1]
                del line[i]
                i = 0
            else:
                i += 1

        for c in reversed(line):
            t *= 5
            t += v[c]
        ts.append(t)

    return sorted(ts)[int(len(ts) / 2)]


if __name__ == "__main__":
    input_file_name = "input.txt"
    # input_file_name = "test2.txt"
    data = list(open(input_file_name))
    print("Part1: " + str(part1(data)))
    print("Part2: " + str(part2(data)))
