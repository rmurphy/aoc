import math


def part2(d):
    tt = 2 ** 32
    for n in range(max(d)):
        t = 0
        for k in d:
            t += (abs(n - k) * (abs(n - k) + 1)) / 2
        if t < tt:
            tt = t
    return int(tt)


def part1(d):
    tt = 2 ** 32
    for n in range(max(d)):
        t = 0
        for k in d:
            t += abs(n - k)
        if t < tt:
            tt = t
    return tt


if __name__ == "__main__":
    d = list(map(int, open("input.txt").readline().split(",")))
    print(part1(d))
    print(part2(d))
