def part1(input_file_name):
    result = 0
    position = [0, 0]
    with open(input_file_name, "r") as input_file:
        for line in input_file:
            direction = line.split()
            if direction[0] == "forward":
                position[0] += int(direction[1])
            elif direction[0] == "down":
                position[1] += int(direction[1])
            elif direction[0] == "up":
                position[1] -= int(direction[1])

    return position[0] * position[1]


def part2(input_file_name):
    result = 0
    position = [0, 0, 0]
    with open(input_file_name, "r") as f:
        for l in f:
            direction = l.split()
            if direction[0] == "forward":
                position[0] += int(direction[1])
                position[1] += position[2] * int(direction[1])
            elif direction[0] == "down":
                position[2] += int(direction[1])
            elif direction[0] == "up":
                position[2] -= int(direction[1])

    return position[0] * position[1]


if __name__ == "__main__":
    input_file_name = "input.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
