def find_mode(l, p, m=1):
    c = 0
    for n in l:
        c += int(n[p])
    if m == 1:
        return str(int(c >= len(l) / 2))
    else:
        return str(int(c < len(l) / 2))


def part1(input_file_name):
    d = list(open("input.txt"))
    q = [0 for i in range(len(d[0]))]
    for n in d:
        q = [a + b for a, b in zip(q, [int(c) for c in n.strip()])]
    s = "".join([str(int((lambda x: x > len(d) / 2)(x))) for x in q])
    z = "".join([str(int(not bool(int(x)))) for x in s])
    return int(s, 2) * int(z, 2)


def part2(input_file_name):
    d = list(open("input.txt"))
    b = d
    p = 0
    while len(d) > 1:
        if p >= len(d[0]) - 1:
            print("not found")
            exit(1)
        m = find_mode(d, p)
        d = list(filter(lambda x: x[p] == m, d))
        p += 1
    p = 0
    while len(b) > 1:
        if p >= len(b[0]) - 1:
            print("not found b")
            exit(1)
        m = find_mode(b, p, 0)
        b = list(filter(lambda x: x[p] == m, b))
        p += 1
    return int(d[0], 2) * int(b[0], 2)


if __name__ == "__main__":
    input_file_name = "input.txt"
    print("Part1: " + str(part1(input_file_name)))
    print("Part2: " + str(part2(input_file_name)))
