import math
import time


def timed(func):
    def wrapper(*args, **kw):
        start = time.time()
        result = func(*args, **kw)
        end = time.time()
        print("Execution of " + func.__name__ + " time in seconds: " + str(end - start))
        return result

    return wrapper


def part1(data):
    low_point_risks = []
    for y, row in enumerate(data):
        for x, col in enumerate(row):
            if is_low_point((x, y), col, data):
                low_point_risks.append(col + 1)
    return sum(low_point_risks)


def part2(data):
    s = set()
    b = []
    for y, row in enumerate(data):
        for x, col in enumerate(row):
            if (x, y) not in s and col != 9:
                b.append(traverse((x, y), data, s))
    return math.prod(sorted(b)[-3:])


def traverse(p, hm, s):
    s.add(p)
    c = 1
    my = len(hm)
    mx = len(hm[0])
    if p[0] + 1 < mx:
        np = (p[0] + 1, p[1])
        if np not in s and hm[p[1]][p[0] + 1] != 9:
            c += traverse(np, hm, s)
    if p[1] + 1 < my:
        np = (p[0], p[1] + 1)
        if np not in s and hm[p[1] + 1][p[0]] != 9:
            c += traverse(np, hm, s)
    if p[0] - 1 >= 0:
        np = (p[0] - 1, p[1])
        if np not in s and hm[p[1]][p[0] - 1] != 9:
            c += traverse(np, hm, s)
    if p[1] - 1 >= 0:
        np = (p[0], p[1] - 1)
        if np not in s and hm[p[1] - 1][p[0]] != 9:
            c += traverse(np, hm, s)
    return c


def is_low_point(p, v, hm):
    for d in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
        ap = tuple(map(sum, zip(p, d)))
        if ap[0] < 0 or ap[1] < 0:
            continue
        try:
            if hm[ap[1]][ap[0]] <= v:
                return False
        except IndexError:
            continue
    return True


@timed
def main():
    input_file_name = "input.txt"
    # input_file_name = "test.txt"
    data = [[int(c) for c in l.strip()] for l in list(open(input_file_name))]
    print("Part1: " + str(part1(data)))
    print("Part2: " + str(part2(data)))


if __name__ == "__main__":
    main()
