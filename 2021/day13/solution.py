import sys


def part1(d):
    fold = {"x": foldx, "y": foldy}
    g = d[0]
    g = fold[d[1][0][0]](g, int(d[1][0][1]))
    c = 0
    for y in range(len(g)):
        for x in range(len(g[0])):
            if g[y][x] == "#":
                c += 1
    return c


def part2(d):
    fold = {"x": foldx, "y": foldy}
    g = d[0]
    for i in d[1]:
        g = fold[i[0]](g, int(i[1]))
    return g


def load_input(fn):
    ps = []
    f = []
    rp = True
    mx = my = 0
    with open(fn, "r") as file:
        for l in file:
            if l.strip() == "":
                rp = False
            else:
                if rp:
                    p = tuple([int(i) for i in l.strip().split(",")])
                    mx = max(mx, p[0])
                    my = max(my, p[1])
                    ps.append(p)
                else:
                    f.append(tuple(l.strip().split(" ")[2].split("=")))
    g = [["."] * (mx + 1) for _ in range(my + 1)]
    for p in ps:
        g[p[1]][p[0]] = "#"
    return g, f


def foldy(g, f):
    ng = []
    for y in range(f):
        nl = g[y][:]
        for x in range(len(g[0])):
            try:
                if g[2 * f - y][x] == "#":
                    nl[x] = "#"
            except IndexError:
                pass
        ng.append(nl)
    return ng


def foldx(g, f):
    ng = []
    for y in range(len(g)):
        nl = g[y][:f]
        for x in range(f):
            if g[y][2 * f - x] == "#":
                nl[x] = "#"
        ng.append(nl)
    return ng


def gp(d):
    for r in d:
        print("".join(r))


def main(input_file_name):
    data = load_input(input_file_name)
    print("Part1 : " + str(part1(data)))
    print("Part2:")
    gp(part2(data))
    # print("Part2: " + str(part2(data)))


if __name__ == "__main__":
    main(sys.argv[1])
