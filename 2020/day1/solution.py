def part1(input_file_name):
    data = set(map(int, open(input_file_name)))
    for n in data:
        if 2020 - n in data:
            return n * (2020 - n)
            break


if __name__ == "__main__":
    input_file_name = "input.txt"
    print("Part1: " + str(part1(input_file_name)))
    # print('Part2: ' + str(part2(input_file_name)))
